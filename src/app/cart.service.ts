import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  product = [];

  addToCart(product) {
    this.product.push(product);
  }

  getItems() {
    return this.product;
  }

  clearCart() {
    this.product = [];
    return this.product;
  }

  constructor() { }
}
