import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { products } from '../products';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  product;
  products = products;
  shoppingCart = [];
  totalPrice;
  helperArray = [];
  productAdded;


  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  calculateTotalPrice() {
    this.totalPrice = 0
    for (var i = 0; i < this.shoppingCart.length; i++) {
      this.totalPrice += this.shoppingCart[i].finalPrice;
    }
    console.log("Precio total del carrito: ", this.totalPrice);
    return this.totalPrice;
  }

  resetShoppingCart() {
    this.shoppingCart = [];
    console.log("Se ha VACIADO el carrito!!: ", this.shoppingCart);
  }

  addToCart(product) {
    this.productAdded = false;
  // Carrito vacío
  if (this.shoppingCart.length === 0) {
    console.log("Carrito VACIO. El producto NO está ya en el carrito, lo añado");
    this.shoppingCart.push(product);
    product.counter = 1;
    product.finalPrice = product.price * product.counter;
  } else if (product.name === 'Strawberries' && product.counter >= 2) {
    this.productAdded = true;
    product.counter++;
    product.price = 4.50;
    product.finalPrice = product.price * product.counter;
  } else if (product.name === 'Coffee' && product.counter >= 2) {
    this.productAdded = true;
    product.counter++;
    product.price = 7.52;
    product.finalPrice = product.price * product.counter;
  } 

  // Carrito con productos
  else {
    for (var i = 0; i < this.shoppingCart.length; i++) {
      if (this.shoppingCart[i].id === product.id) {
        this.productAdded = true;
        console.log("El producto SI está en el carrito, sumo contador");
        product.counter++;
        product.finalPrice = product.price * product.counter;
       } 
    }
    // Producto nuevo del carrito
    if (this.productAdded === false) {
      console.log("El producto NO está ya en el carrito, lo añado");
      this.shoppingCart.push(product);
      product.counter = 1;
      console.log("Producto añadido al carrito!!", product);
    } 
  }
  console.log("El carrito está así", this.shoppingCart, "y este es el precio", this.totalPrice);
  }

  removeProduct(product) {
   console.log("El carrito ANTES de eliminar producto", this.shoppingCart);
    for (var i = 0; i < this.shoppingCart.length; i++) {
      if (this.shoppingCart[i].id === product.id) {
        if (product.counter == 1){
        this.shoppingCart.splice(i, 1);
        product.finalPrice = product.price * product.counter;
        }
        else if (product.counter > 1){
          this.shoppingCart[i].counter--;
          product.finalPrice = product.price * product.counter;
        } 
    } else (product.counter <= 2); {
      if (product.name === 'Strawberries' && product.counter <= 2) {
        product.price = 5;
        product.finalPrice = product.price * product.counter;
      } 
      else if (product.name === 'Coffee' && product.counter <= 2) {
        product.price = 11.23;
        product.finalPrice = product.price * product.counter;
      }
    } 
    console.log("El carrito DESPUES de eliminar producto", this.shoppingCart);
  }
}

  
}
