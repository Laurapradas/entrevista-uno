export const products = [
    {
      name: 'Green Tea',
      price: 3.11,
      id: 'GR1',   
    },
    {
      name: 'Strawberries',
      price: 5.00,
      id: 'SR1'
    },
    {
      name: 'Coffee',
      price: 11.23,
      id: 'CF1'
    }
  ];